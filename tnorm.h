/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_TNORM_H
#define uF_TNORM_H

/*==================[inclusions]=============================================*/
#include "fixed_point.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/

/** \brief Minimum t-norm index. */
#define uF_TNORM_MINIMUM 	0
/** \brief Algebraic t-norm index. */
#define uF_TNORM_ALGEBRAIC	1
/** \brief Bounded t-norm index. */
#define uF_TNORM_BOUNDED	2
/** \brief Drastic t-norm index. */
#define uF_TNORM_DRASTIC	3
/** \brief Einstein t-norm index. */
#define uF_TNORM_EINSTEIN	4
/** \brief Hamacher t-norm index. */
#define uF_TNORM_HAMACHER	5

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/** \brief Array of t-norm function pointers. */
extern uF_FP (*uF_TNORMS[])(uF_FP a, uF_FP b);

/*==================[external functions declaration]=========================*/

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif
/*==================[end of file]============================================*/
#endif /* uF_TNORM_H */
