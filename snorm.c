/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/
#include "snorm.h"
#include "fixed_point.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** \brief Calculates the minimum s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_max(uF_FP a, uF_FP b);

/** \brief Calculates the algebraic s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_algebraic(uF_FP a, uF_FP b);

/** \brief Calculates the bounded s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_bounded(uF_FP a, uF_FP b);

/** \brief Calculates the drastic s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_drastic(uF_FP a, uF_FP b);

/** \brief Calculates the einstein s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_einstein(uF_FP a, uF_FP b);

/** \brief Calculates the hamacher s-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The s-norm between a and b.
 **/
static uF_FP uF_snorm_hamacher(uF_FP a, uF_FP b);
/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
uF_FP (*uF_SNORMS[])(uF_FP a, uF_FP b) =
{
   uF_snorm_max,
   uF_snorm_algebraic,
   uF_snorm_bounded,
   uF_snorm_drastic,
   uF_snorm_einstein,
   uF_snorm_hamacher
};

/*==================[internal functions definition]==========================*/
static uF_FP uF_snorm_max(uF_FP a, uF_FP b)
{
   return uF_Max(a, b);
}

static uF_FP uF_snorm_algebraic(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE sum = (uF_FP_DOUBLE) a + b;
   uF_FP_DOUBLE prod = uF_Mul(a, b);
   return sum - prod;
}

static uF_FP uF_snorm_bounded(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE sum = (uF_FP_DOUBLE) a + b;
   return uF_Min(sum, uF_FP_UNIT);
}

static uF_FP uF_snorm_drastic(uF_FP a, uF_FP b)
{
   return (uF_Min(a, b)) ? uF_FP_UNIT : uF_Max(a, b);
}

static uF_FP uF_snorm_einstein(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE den = (uF_FP_UNIT + (uF_FP_DOUBLE) uF_Mul(a, b));
   uF_FP_DOUBLE num = (uF_FP_DOUBLE) a + b;
   return uF_Div(num, den);
}

static uF_FP uF_snorm_hamacher(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE prod = uF_Mul(a, b);
   uF_FP_DOUBLE den = (uF_FP_UNIT - prod);
   uF_FP_DOUBLE num = (uF_FP_DOUBLE) a + b - uF_M2n(prod, 1);
   if (den != 0)
   {
      return uF_Div(num, den);
   }
   return uF_FP_UNIT;
}

/*==================[external functions definition]==========================*/

/*==================[end of file]============================================*/
