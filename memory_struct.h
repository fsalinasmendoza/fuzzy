#ifndef uF_MEMORY_STRUCT_H
#define uF_MEMORY_STRUCT_H
#include "controller.h"
#include "config.h"

#define TABLE_SIZE   (0x10000 >> (16 - uF_MEM_FUNC_TABLE_INDEX_SIZE))

#pragma pack(push)
#pragma pack(1)
#pragma options align=packed
typedef struct controller_struct_type
{
   uint8_t signature[3];
   uint8_t version;
   uint8_t table_index_size;
   uint8_t operation_and;
   uint8_t operation_or;
   uint8_t operation_activation;
   uint8_t operation_aggregation;
   uint8_t operation_defuzzification;
   uint8_t sensor_num;
   uint8_t actuator_num;
} controller_struct_type;

typedef struct
{
   uint8_t membership_function_num;
} sensor_struct_type;

typedef struct
{
   uint8_t type;
} membership_function_struct_type;

typedef struct
{
   uint16_t point_x[4];
} trapezoid_struct_type;

typedef struct
{
   uint16_t point_y[TABLE_SIZE];
} table_function_struct_type;

typedef struct
{
   uint8_t inference_type;
   uint8_t membership_function_num;
} actuator_struct_type;

typedef struct ruleset_struct_type
{
   uint8_t inference;
   uint8_t rule_num;
} ruleset_struct_type;

#pragma pack(pop)

#endif // uF_MEMORY_STRUCT_H
