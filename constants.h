#ifndef uF_CONSTANTS_H
#define uF_CONSTANTS_H

#ifndef NULL
#define NULL 0
#endif

#if !defined( uF_SENSOR_NUM )
#error "uF_SENSOR_NUM no esta definido"
#elif uF_SENSOR_NUM > 16
#error "uF_SENSOR_NUM debe ser menor o igual a 16"
#endif

#if !defined( uF_ACTUATOR_NUM )
#error "uF_ACTUATOR_NUM no esta definido"
#elif uF_ACTUATOR_NUM > 16
#error "uF_ACTUATOR_NUM debe ser menor o igual a 16"
#endif

#if !defined( uF_SENSOR_MF_NUM )
#error "uF_SENSOR_MF_NUM no esta definido"
#elif uF_SENSOR_MF_NUM > 15
#error "uF_SENSOR_MF_NUM debe ser menor o igual a 15"
#endif

#if !defined( uF_ACTUATOR_MF_NUM )
#error "uF_ACTUATOR_MF_NUM no esta definido"
#elif uF_ACTUATOR_MF_NUM > 15
#error "uF_ACTUATOR_MF_NUM debe ser menor o igual a 15"
#endif

/** \brief Mamdani inference type
 **/
#define uF_MAMDANI	1

/** \brief Sugeno inference type
 **/
#define uF_SUGENO	2

#endif // uF_CONSTANTS_H
