/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/
#include "mem_func_table.h"

/* Number of sensors, actuators, etc. */
#include "config.h"
#include "fixed_point.h"
#include "mem_func.h"
/* Ntohs. */
#include "utilities.h"

/*==================[macros and definitions]=================================*/
/** \brief Maximum possible number of membership functions. */
#define MAX_MEM_FUNC (uF_ACTUATOR_NUM * uF_ACTUATOR_MF_NUM + uF_SENSOR_NUM * uF_SENSOR_MF_NUM)

/** \brief Displacement that must by applied to a real number in [0,1) to use
 ** it to access a membership function table.
 **/
#define TABLE_INDEX_OFFSET    (uF_FP_FRACTIONAL_BITS - uF_MEM_FUNC_TABLE_INDEX_SIZE)
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/** \brief Membership function pool. */
static mem_func_table_type _block[MAX_MEM_FUNC];

/** \brief Number of membership functions created. */
static int mem_func_counter = 0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

mem_func_table_type* uF_new_table(table_function_struct_type *gs)
{
   mem_func_table_type *mf;

   if (mem_func_counter >= MAX_MEM_FUNC)
   {
      return NULL;
   }

   mf = &_block[mem_func_counter++];

   mf->super.eval = uF_eval_table;
   mf->super.del = uF_delete_table;
   mf->point_y = gs->point_y;
   return mf;
}
void uF_delete_table(void *mf)
{

}

uF_FP uF_eval_table(void *mf, uF_FP x)
{
   int_fast16_t index;
   if (x < 0 || x >= uF_FP_UNIT)
   {
      return -1;
   }
   index = uF_D2n(x, TABLE_INDEX_OFFSET);
   return (uF_FP) uF_Ntohs(((mem_func_table_type*) mf)->point_y[index]);
}

/*==================[end of file]============================================*/
