/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/
#include "fixed_point.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
short uF_FpToInt(uF_FP fp, short range_min, short range_max)
{

   return (short) (((fp * ((long) range_max - range_min)) / (uF_FP_UNIT - 1)) + range_min);
}

uF_FP uF_IntToFp(short i, short range_min, short range_max)
{
   return (uF_FP) (((long) i - range_min) * (uF_FP_UNIT - 1)) / ((long) range_max - range_min);
}

long uF_FpToLongInt(uF_FP fp, long range_min, long range_max)
{
   long long range_dif = ((long long) range_max) - range_min;
   long long num = fp * range_dif;
   return (long) (num / (uF_FP_UNIT - 1ll) + range_min);
}

uF_FP uF_LongIntToFp(long i, long range_min, long range_max)
{
   long long range_dif = ((long long) range_max) - range_min;
   long long x = ((long long) i) - range_min;
   long long num = x * (uF_FP_UNIT - 1);
   return (uF_FP) (num / range_dif);
}
/*==================[end of file]============================================*/
