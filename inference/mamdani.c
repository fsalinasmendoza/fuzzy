/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/

#include "mamdani.h"
#include "../actuator.h"
/* Actuator number. */
#include "../config.h"
#include "../controller.h"
#include "../datatypes.h"
#include "../fixed_point.h"
#include "../memory_struct.h"
#include "mamdani/defuzzify.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** \brief Calculates the the triggering strength for all the membership
 ** functions of the given actuator for the current input state.
 ** 
 ** \param controller Pointer to the controller being used 
 ** \param actuator_id Number of the actuator
 ** \param[out] mf_strenght Array with the strength for each membership function
 **/
static void getTriggeringStrengths(controller_type *controller,
      actuator_type *actuator, uF_FP *mf_strenght)
{
   ruleset_type *ruleset = (ruleset_type *) (&actuator->ruleset);
   uF_FP *input_weights = controller->input_weights;
   uint8_t *mem_func_id;
   uF_FP weight;
   uint_fast8_t *input_offset = controller->input_offsets;
   uint_fast8_t sensor_num = controller->config->sensor_num;
   uint_fast8_t i, j;
   uint_fast8_t out_mf;

   /* clear the output array */
   for (i = 0; i < actuator->mem_func_num; ++i)
   {
      mf_strenght[i] = 0;
   }

   /* array to the membership functions involved in a given rule for each input variable */
   mem_func_id = ruleset->data;

   /* for each rule */
   for (i = 0; i < ruleset->rule_num; ++i)
   {

      /* assign the input_offset to a local variable to improve performance */
      input_offset = controller->input_offsets;

      /* assign the maximum value to the rule weight */
      weight = uF_FP_UNIT;

      /* for each input variable */
      for (j = 0; j < sensor_num; ++j, ++mem_func_id, ++input_offset)
      {

         /* if the input variable appears in the rule */
         if (*mem_func_id != 0xFFu)
         {
            /* then accumulate its weight */
            weight = uF_and(weight,
                  input_weights[*mem_func_id + *input_offset]);
         }
         /* else the input variable does not appear in the rule, i.e. must
          * be ignored for this calculation */
      }
      /* the output membership function is the last one */
      out_mf = *mem_func_id;

      /* move the pointer to the start of the next rule */
      ++mem_func_id;

      /* accumulate the calculated weight to its position in the array */
      mf_strenght[out_mf] = uF_or(mf_strenght[out_mf], weight);
   }

   return;
}

/*==================[external functions definition]==========================*/

uF_FP uF_mamdani(controller_type *controller, actuator_type *actuator,
      uF_FP *is)
{
   uF_FP mf_strengths[uF_ACTUATOR_MF_NUM];

   /* Obtain the triggering strength for the output membership functions */
   getTriggeringStrengths(controller, actuator, mf_strengths);

   switch (controller->config->operation_defuzzification)
   {
   default:
      return uF_defuzzify_CoG(actuator, mf_strengths);
      break;
   }
}

/*==================[end of file]============================================*/
