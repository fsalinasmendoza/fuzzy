/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_SUGENO_H
#define uF_SUGENO_H

/*==================[inclusions]=============================================*/
#include "../fixed_point.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef struct controller_type controller_type;
typedef struct actuator_type actuator_type;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** \brief Calculates the output value using the Sugeno inference method.
 **
 ** \param controller Pointer to the controller being used
 ** \param actuator_id Number of the actuator
 ** \param is Array with the input values
 ** \return Output value for the given input set
 **/
uF_FP uF_sugeno(controller_type* controller, actuator_type *actuator,
      uF_FP *is);

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* uF_SUGENO_H */
