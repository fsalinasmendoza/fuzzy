/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/

#include "sugeno.h"
#include "../actuator.h"
#include "../controller.h"
#include "../datatypes.h"
#include "../fixed_point.h"
#include "../memory_struct.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static uF_FP processInputs(controller_type *controller, actuator_type *actuator,
      uF_FP *is)
{
   ruleset_type *rs = (ruleset_type *) (&actuator->ruleset);
   uF_FP *input_weights = controller->input_weights;
   uint_fast8_t *input_offset = controller->input_offsets;
   /* array with the membership function identifier associated with each input for a given rule */
   uint8_t* mem_func_id;
   /* array with output function coefficients */
   int16_t *coefficients;
   uF_FP_DOUBLE weight_sum;
   uF_FP_DOUBLE value_sum;
   /* holds the output value */
   uF_FP_DOUBLE output_value;
   uint_fast8_t sensor_num = controller->config->sensor_num;
   uint_fast8_t i, j;
   uF_FP weight;

   weight_sum = 0;
   value_sum = 0;

   /* for each rule */
   for (i = 0; i < rs->rule_num; ++i)
   {
      /* pointer to the membership function id for each term of the antecedent */
      mem_func_id = rs->data + i * (sensor_num + ((sensor_num + 1) << 1));
      /* pointer to the coefficients for each term of the consequent */
      coefficients = (int16_t *) (mem_func_id + sensor_num);

      /* assign to temporary variable */
      input_offset = controller->input_offsets;

      /* initialize the calculated weight for the rule with logical 1 */
      weight = uF_FP_UNIT;

      /* initialize the output value with the independent term */
      output_value = uF_Ntohs(*(coefficients++));

      /* for each sensor */
      for (j = 0; j < sensor_num;
            ++j, ++mem_func_id, ++input_offset, ++coefficients)
      {
         /* calculate the partial output weight */

         /* if the id is not 255 */
         if (*mem_func_id != 0xFFu)
         {
            /* then compute its weight */
            weight = uF_and(weight,
                  input_weights[*mem_func_id + *input_offset]);
         }
         /* else the input variable is ignored for this rule */

         /* calculate the partial output value */
         uF_FP tmp = uF_Ntohs(*coefficients);
         output_value += uF_Mul(is[j], tmp);

      }

      value_sum += uF_Mul(output_value, weight);
      weight_sum += weight;
   }

   return
         (weight_sum != 0) ?
               uF_Div(value_sum, weight_sum) : uF_D2n(uF_FP_UNIT, 1);
}

/*==================[external functions definition]==========================*/

uF_FP uF_sugeno(controller_type* controller, actuator_type *actuator, uF_FP *is)
{
   return processInputs(controller, actuator, is);
}

/*==================[end of file]============================================*/

