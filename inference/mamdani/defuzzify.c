/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/

#include "defuzzify.h"
#include "../../actuator.h"
#include "../../config.h"
#include "../../datatypes.h"
#include "../../fixed_point.h"
#include "../../snorm.h"
#include "../../tnorm.h"
#include "../../mem_func.h"

/*==================[macros and definitions]=================================*/

#define CRISP_VALUE_STEP (1 << (uF_FP_FRACTIONAL_BITS - uF_MEM_FUNC_TABLE_INDEX_SIZE))

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uF_FP uF_defuzzify_CoG(actuator_type *actuator, uF_FP *mem_func_strengths)
{
   uF_FP_DOUBLE value = 0;
   uF_FP_DOUBLE sum_weight = 0;
   uF_FP i = 0;
   uF_FP wti;
   uint_fast8_t mem_func_index;

   do
   {
      wti = 0;
      for (mem_func_index = 0; mem_func_index < actuator->mem_func_num;
            mem_func_index++)
      {
         wti = uF_aggregation(wti,
               uF_activation(mem_func_strengths[mem_func_index],
                     uF_eval(actuator->mf[mem_func_index], i)));
      }

      value += uF_Mul(i, wti);
      sum_weight += wti;

      i += CRISP_VALUE_STEP;
   } while (i < uF_FP_UNIT);

   /* perform the division and return the value */
   return (sum_weight != 0) ? uF_Div(value, sum_weight) : uF_D2n(uF_FP_UNIT, 1);
}

/*==================[end of file]============================================*/
