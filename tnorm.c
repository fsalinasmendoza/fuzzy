/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/
#include "tnorm.h"
#include "fixed_point.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/** \brief Calculates the minimum t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_min(uF_FP a, uF_FP b);

/** \brief Calculates the algebraic t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_algebraic(uF_FP a, uF_FP b);

/** \brief Calculates the bounded t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_bounded(uF_FP a, uF_FP b);

/** \brief Calculates the drastic t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_drastic(uF_FP a, uF_FP b);

/** \brief Calculates the einstein t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_einstein(uF_FP a, uF_FP b);

/** \brief Calculates the hamacher t-norm between a and b.
 **
 ** The parameters a and b must belong to [0, 1]. This function does not
 ** perform any verification on these values.
 ** \param a Number in [0, 1].
 ** \param b Number in [0, 1].
 ** \return The t-norm between a and b.
 **/
static uF_FP uF_tnorm_hamacher(uF_FP a, uF_FP b);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

uF_FP (*uF_TNORMS[])(uF_FP a, uF_FP b) =
{
   uF_tnorm_min,
   uF_tnorm_algebraic,
   uF_tnorm_bounded,
   uF_tnorm_drastic,
   uF_tnorm_einstein,
   uF_tnorm_hamacher
};
/*==================[internal functions definition]==========================*/

static uF_FP uF_tnorm_min(uF_FP a, uF_FP b)
{
   return uF_Min(a, b);
}

static uF_FP uF_tnorm_algebraic(uF_FP a, uF_FP b)
{
   return uF_Mul(a, b);
}

static uF_FP uF_tnorm_bounded(uF_FP a, uF_FP b)
{
   return (a + b <= uF_FP_UNIT) ? 0 : a + b - uF_FP_UNIT;
}

static uF_FP uF_tnorm_drastic(uF_FP a, uF_FP b)
{
   if (b == uF_FP_UNIT)
   {
      return a;
   } else
   {
      if (a == uF_FP_UNIT)
      {
         return b;
      } else
      {
         return 0;
      }
   }
}

static uF_FP uF_tnorm_einstein(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE prod = uF_Mul(a, b);
   uF_FP_DOUBLE sum = (uF_FP_DOUBLE) a + b;
   return uF_Div(prod, uF_FPMake(2) - (sum - prod));
}

static uF_FP uF_tnorm_hamacher(uF_FP a, uF_FP b)
{
   uF_FP_DOUBLE prod = uF_Mul(a, b);
   uF_FP_DOUBLE sum = (uF_FP_DOUBLE) a + b;
   if (sum != prod)
   {
      return uF_Div(prod, (sum - prod));
   }
   return 0;
}
/*==================[external functions definition]==========================*/

/*==================[end of file]============================================*/
