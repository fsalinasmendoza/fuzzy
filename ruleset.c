#include "ruleset.h"
#include "datatypes.h"
#include "actuator.h"
#include "memory_struct.h"

int_fast8_t uF_ruleset_mamdani_init(ruleset_type *ruleset, uint8_t *data,
      uint_fast8_t sensor_num, uint_fast8_t actuator_num)
{
   int_fast8_t ret = -1;

   /* initialize the fields */
   ruleset->rule_num = *(data++);
   ruleset->data = data;

   /* increment the pointer considering 1 byte for each consequent */
   ret = uF_RULESET_HEADER_SIZE + ruleset->rule_num * (sensor_num + 1);

   return ret;
}

int_fast8_t uF_ruleset_sugeno_init(ruleset_type *ruleset, uint8_t *data,
      uint_fast8_t sensor_num, uint_fast8_t actuator_num)
{
   int_fast8_t ret = -1;

   /* initialize the fields */
   ruleset->rule_num = *(data++);
   ruleset->data = data;

   /* increment the pointer considering sensor_num + 1 bytes for each consequent */
   ret = uF_RULESET_HEADER_SIZE + ruleset->rule_num * (sensor_num + ((sensor_num + 1) << 1));

   return ret;
}
