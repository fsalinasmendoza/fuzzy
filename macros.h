#ifndef uF_MACROS_H
#define uF_MACROS_H

#include "config.h"
#include "datatypes.h"
#include "constants.h"

#if defined(uF_DEBUG)
#include <assert.h>
#define uF_assert(a) assert(a)
#else
#define uF_assert(a) ((void) 0)
#endif

#endif
