/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/

#include "config.h"
#include "datatypes.h"
#include "controller.h"
#include "actuator.h"
#include "sensor.h"
#include "fixed_point.h"
#include "ruleset.h"
#include "mem_func.h"
#include "mem_func_trapezoid.h"
#include "mem_func_table.h"
#include "snorm.h"
#include "tnorm.h"
#include "utilities.h"
#include "inference/sugeno.h"
#include "inference/mamdani.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/** \brief Fuzzifies the given input state using the singleton method.
 **
 ** Calculates the weights of all the input membership functions and returns
 ** them into the controller input_weights field as an array.
 **
 ** \param controller Pointer to the controller being used.
 ** \param is Array with the readings of the sensors.
 **/
static void fuzzify(controller_type* controller, uF_FP* is)
{
   /* array with the sensors */
   sensor_type* sensors = controller->sensors;
   /* array with the number of membership functions for each sensor */
   uint_fast8_t* mem_func_num = controller->mem_func_num;
   /* array with the weights for the given input state for all the membership
    * function for all the sensors */
   uF_FP* mem_func_weigths;
   /* array with the membership functions for a given sensor */
   mem_func_type** mem_funcs = sensors[0].mf;
   /* total number of sensors */
   uint8_t sensor_num = controller->config->sensor_num;
   /* reading of the current sensor */
   uF_FP sensor_reading;
   /* index to go through the membership functions array */
   uint8_t mem_func_index;
   /* index to go through the sensors array */
   uint8_t sensor_index;

   /* point to the begining of the output array */
   mem_func_weigths = controller->input_weights;

   /* use the first sensor reading */
   sensor_reading = is[0];

   /* for each sensor */
   for (sensor_index = 0, mem_func_index = 0; sensor_index < sensor_num;
         ++mem_func_index, ++mem_func_weigths)
   {
      /* evaluate every membership function */

      /* if all the membership functions have been evaluated */
      if (mem_func_index >= mem_func_num[sensor_index])
      {
         /* then start with the next sensor */

         /* if all the sensors have been evaluated */
         if (++sensor_index >= sensor_num)
         {
            /* then finish */
            break;
         } else
         {
            /* else start with the first membership function for the
             * current sensor */
            mem_func_index = 0;

            /* use the corresponding sensor reading */
            sensor_reading = is[sensor_index];

            /* point to the membership function array of the next sensor */
            mem_funcs = sensors[sensor_index].mf;
         }
      }

      /* evaluate the current membership function with the corresponding sensor
       * reading */
      *mem_func_weigths = uF_eval(mem_funcs[mem_func_index], sensor_reading);
   }
}

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
uF_FP (*uF_and)(uF_FP, uF_FP);
uF_FP (*uF_or)(uF_FP, uF_FP);
uF_FP (*uF_activation)(uF_FP, uF_FP);
uF_FP (*uF_aggregation)(uF_FP, uF_FP);

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

controller_type *uF_controller_init(void *controller_struct)
{
   controller_type *controller;
   /* generic actuator pointer */
   actuator_type *actuator;
   controller_struct_type *header;
   uint8_t *cs;
   uint8_t *signature;
   uint_fast16_t table_size;
   uint_fast8_t ret = 0;
   uint_fast8_t actuator_num;
   uint_fast8_t sensor_num;
   uint_fast8_t i, j;
   uint_fast8_t type;
   static controller_type _block;
   controller = &_block;

   cs = (uint8_t *) controller_struct;
   header = (controller_struct_type *) controller_struct;

   signature = header->signature;
   if (signature[0] != 'C' || signature[1] != 'F' || signature[2] != 'S')
   {
      uF_print( "\nDatos corruptos. El bloque no tiene formato cfs" );
      return NULL;
   }

   if (header->table_index_size != uF_MEM_FUNC_TABLE_INDEX_SIZE)
   {
      uF_print("\nThe table index size must be at most " uF_STRING(uF_MEM_FUNC_TABLE_INDEX_SIZE));
      return NULL;
   }

   table_size = 1 << header->table_index_size;

   actuator_num = header->actuator_num;
   sensor_num = header->sensor_num;

   if (actuator_num > uF_ACTUATOR_NUM)
   {
      uF_print( "\nDatos corruptos. Debe haber entre 1 y " uF_STRING( uF_ACTUATOR_NUM ) " actuadores" );
      return NULL;
   }

   if (sensor_num > uF_SENSOR_NUM)
   {
      uF_print( "\nDatos corruptos. Debe haber entre 1 y " uF_STRING( uF_SENSOR_NUM ) " sensores" );
      return NULL;
   }

   controller->config = (controller_struct_type*) controller_struct;

   //Determinamos operaciones a utilizar
   uF_and = uF_TNORMS[controller->config->operation_and];
   uF_or = uF_SNORMS[controller->config->operation_or];
   uF_activation = uF_TNORMS[controller->config->operation_activation];
   uF_aggregation = uF_SNORMS[controller->config->operation_aggregation];

   cs += sizeof(controller_struct_type); //cs ya apunta a sensor_struct

   i = 0;
   j = 0;

   for (i = 0; i < sensor_num; ++i)
   {
      int mf_num = controller->sensors[i].mem_func_num =
            ((sensor_struct_type*) cs)->membership_function_num;

      if (mf_num > uF_SENSOR_MF_NUM)
      {
         uF_print( "\nDatos corruptos. Debe haber entre 1 y " uF_STRING( uF_SENSOR_MF_NUM ) " funciones de membresia para sensores" );
         return NULL;
      }

      cs += sizeof(sensor_struct_type);
      for (j = 0; j < mf_num; ++j)
      {
         type = ((membership_function_struct_type*) cs)->type;
         if (0 == type)
         { // TRAPEZOID
            cs += sizeof(membership_function_struct_type);
            controller->sensors[i].mf[j] = (mem_func_type*) uF_new_trapezoid(
                  (trapezoid_struct_type*) cs);
            cs += sizeof(trapezoid_struct_type);
         } else if (1 == type)
         { // GENERIC

#ifndef uF_SENSOR_TRAPEZOID_ONLY
            cs = cs + sizeof(membership_function_struct_type);
            controller->sensors[i].mf[j] = (mem_func_type*) uF_new_table(
                  (table_function_struct_type*) cs);
            cs += sizeof(table_function_struct_type);
#else
            uF_print( "\nSolo se permiten funciones de membresia trapezoidales para sensores" );
            return NULL;
#endif // uF_SENSOR_TRAPEZOID_ONLY
         } else
         {
            uF_print( "\nDatos corruptos. Funcion de membresia no reconocida" );
            return NULL;
         }
      }
   }

   for (i = 0; i < actuator_num; ++i)
   {
      actuator = &controller->actuators[i];

      actuator->inference_type = ((actuator_struct_type*) cs)->inference_type;

      cs += sizeof(((actuator_struct_type*) cs)->inference_type);

      if (actuator->inference_type == uF_MAMDANI)
      {
         int mf_num = actuator->mem_func_num =
               ((actuator_struct_type*) cs)->membership_function_num;

         if (mf_num > uF_ACTUATOR_MF_NUM)
         {
            uF_print("\nDatos corruptos. Debe haber entre 1 y " uF_STRING(uF_ACTUATOR_MF_NUM) " funciones de membresia para actuadores");
            return NULL;
         }
         cs += sizeof(((actuator_struct_type*) cs)->membership_function_num);

         for (j = 0; j < mf_num; ++j)
         {
            type = ((membership_function_struct_type*) cs)->type;
            if (0 == type)
            { // TRAPEZOID
               cs = cs + sizeof(membership_function_struct_type);
               actuator->mf[j] = (mem_func_type*) uF_new_trapezoid(
                     (trapezoid_struct_type*) cs);
               cs += sizeof(trapezoid_struct_type);
            } else if (1 == type)
            { // GENERIC
#ifndef uF_ACTUATOR_TRAPEZOID_ONLY
               cs = cs + sizeof(membership_function_struct_type);
               actuator->mf[j] = (mem_func_type*) uF_new_table(
                     (table_function_struct_type*) cs);
               cs += sizeof(table_function_struct_type);
#else
               uF_print( "\nSolo se permiten funciones de membresia trapezoidales para actuadores" );
               return NULL;
#endif
            } else
            {
               uF_print("\nDatos corruptos. Funcion de membresia no reconocida");
               return NULL;
            }

         }

      }

      /* initialize the ruleset */
      if (actuator->inference_type == uF_MAMDANI)
      {
         ret = uF_ruleset_mamdani_init(&actuator->ruleset, cs, sensor_num,
               actuator_num);
      } else if (actuator->inference_type == uF_SUGENO)
      {
         ret = uF_ruleset_sugeno_init(&actuator->ruleset, cs, sensor_num,
               actuator_num);
      } else
      {
         uF_print("\nTipo de inferencia no reconocido");
         return NULL;
      }
      uF_assert(ret >= 0);

      /* increment the data pointer */
      cs += ret;
   }

   // Inicializacion de los arreglos

   // obtengo la cantidad de funciones de membresia para cada sensor
   {
      uint_fast8_t* mem_func_num = controller->mem_func_num;
      for (i = 0; i < sensor_num; ++i)
      {
         mem_func_num[i] = controller->sensors[i].mem_func_num;
      }
   }

   // offsets para acceder al array input_weights
   {
      uint_fast8_t* input_offsets = controller->input_offsets;
      input_offsets[0] = 0;
      for (i = 1; i < sensor_num; ++i)
         input_offsets[i] = input_offsets[i - 1]
               + controller->sensors[i - 1].mem_func_num;
   }

   return controller;
}

void uF_controller_deinit(controller_type* controller)
{
}

uF_FP uF_process_actuator(controller_type* controller, int actuator_id,
      uF_FP* is)
{
   actuator_type* actuator = &controller->actuators[actuator_id];

   uF_FP value = 0;

   if (actuator->inference_type == uF_SUGENO)
   {
      value = uF_sugeno(controller, actuator, is);
   } else
   {
      value = uF_mamdani(controller, actuator, is);
   }
   return value;
}

int uF_process(controller_type *controller, uF_FP *inputs, int inputs_size,
      uF_FP *outputs, int outputs_size)
{
   /* number of actuators */
   uint_fast8_t actuator_num;
   /* actuator id */
   uint_fast8_t actuator_id;

   /* get the number of actuators of the controller */
   actuator_num = controller->config->actuator_num;

   if (inputs_size < controller->config->sensor_num
         || outputs_size < actuator_num)
   {
      return -1;
   }

   /* fuzzify the inputs */
   fuzzify(controller, inputs);

   /* for each actuator */
   for (actuator_id = 0; actuator_id < actuator_num; ++actuator_id)
   {
      /* calculate its output */
      outputs[actuator_id] = uF_process_actuator(controller, actuator_id,
            inputs);
   }

   return 0;
}

/*==================[end of file]============================================*/
