#ifndef uF_SENSOR_H
#define uF_SENSOR_H

#include "controller.h"

typedef struct mem_func_type mem_func_type;

typedef struct sensor_type
{
   mem_func_type *mf[uF_SENSOR_MF_NUM];
   uint_fast8_t mem_func_num;
} sensor_type;

#endif // uF_SENSOR_H
