#ifndef uF_CONFIG_H
#define uF_CONFIG_H

/** \file
 *  En este archivo reside la configuraci&oacute;n del controlador. Es necesario definir ciertas macros.
 *  <H1> Configurac&oacute;n </H1>
 *	- uF_STATIC: si est&aacute; definida, el controlador no reserva memoria din&aacute;micamente. La memoria necesaria para la ejecuci&oacute;n queda determinada en tiempo de compilaci&oacute;n, en funci&oacute;n de la cantidad de sensores, actuadores y otras variables. Si se usa la opci&oacute;n uF_STATIC deben definirse las siguientes macros:
 *		- uF_SENSOR_NUM: cantidad m&aacute;xima de sensores o variables de entrada.
 *		- uF_ACTUATOR_NUM: cantidad m&aacute;xima de actuadores o variables de salida.
 *		- uF_SENSOR_MF_NUM: cantidad m&aacute;xima de funciones de membres&iacute;a de cada variable de entrada. Cuando se usa uF_STATIC todas las variables de entrada tienen la misma cantidad de funciones de membres&iacute;a.
 *		- uF_ACTUATOR_MF_NUM: cantidad m&aacute;xima de funciones de membres&iacute;a de cada variable de salida. Cuando se usa uF_STATIC todas las variables de salida tienen la misma cantidad de funciones de membres&iacute;a.
 *	- uF_SENSOR_TRAPEZOID_ONLY: Debe definirse cuando se utilizan �nicamente funciones de membres�a triangulares o trapezoidales para los sensores. Produce un ahorro significativo de memoria, sin afectar la performance.
 *	- uF_ACTUATOR_TRAPEZOID_ONLY: Puede definirse cuando se utilizan �nicamente funciones de membres�a triangulares o trapezoidales para los actuadores. Produce un ahorro significativo de memoria aunque el procesamiento es m�s lento.
 *	- uF_NO_PRINT: Si se define, se omiten los mensajes por consola.
 *  <H1> Definiciones de tipos enteros.</H1>
 *  Deben definirse los tipos de dato enteros que ser&aacute;n usados por el controlador. Esto es necesario s&oacute;lo en caso de no estar disponible &lt;stdint.h&gt;. Si est&aacute; disponible entonces estas macros son definidas autom&aacute;ticamente.
 *	- uF_UINT8: tipo de datos de sin signo de 8 bits. uint8_type por defecto.
 *	- uF_UINT16: tipo de datos de sin signo de 16 bits. uint16_type por defecto.
 *	- uF_UINT32: tipo de datos de sin signo de 32 bits. uint32_type por defecto.
 *	- uF_SINT16: tipo de datos de con signo de 16 bits. sint16_type por defecto.
 *	- uF_SINT32: tipo de datos de con signo de 32 bits. sint32_type por defecto.
 */

#define uF_NO_LIBS

#define uF_STATIC

#if 0
#define uF_DEBUG
#endif

#define uF_SENSOR_NUM       3
#define uF_SENSOR_MF_NUM    6
#define uF_ACTUATOR_NUM     2
#define uF_ACTUATOR_MF_NUM  4

#define uF_RULE_NUM			8
#define uF_MEM_FUNC_TABLE_INDEX_SIZE   8

#endif // uF_CONFIG_H
