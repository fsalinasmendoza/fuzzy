/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_MEM_FUNC_H
#define uF_MEM_FUNC_H

/*==================[inclusions]=============================================*/
#include "fixed_point.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/** \brief Membership function type. */
typedef struct mem_func_type
{
   /** \brief Callback to delete. */
   void (*del)();

   /** \brief Callback to evaluate. */
   uF_FP (*eval)(void*, uF_FP);
} mem_func_type;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** \brief Evaluates the membership function.
 **
 ** \param mf Pointer to the membership function structure.
 ** \param x Crisp value in [0, 1).
 ** \return The degree of membership of v to the given membership function.
 **/
uF_FP uF_eval(void* mf, uF_FP x);

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* uF_MEM_FUNC_H */

