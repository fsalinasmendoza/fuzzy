/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_FIXED_POINT_H
#define uF_FIXED_POINT_H

/*==================[inclusions]=============================================*/
#include "datatypes.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/

/** \brief Number of bits for the fractional part */
#define uF_FP_FRACTIONAL_BITS		14

/** \brief Maximum value for the real type. */
#define uF_FP_MAX       0x7FFF

/** \brief Minimum value for the real type. */
#define uF_FP_MIN       0x8000

/** \brief Multiplies two numbers. */
#define uF_Mul(a, b)    (((int_fast32_t) (a) * (b)) >> uF_FP_FRACTIONAL_BITS)

/** \brief Divides two numbers. */
#define uF_Div(a, b)    (((int_fast32_t) (a) << uF_FP_FRACTIONAL_BITS) / (b))

/** \brief Calculates the reminder of a division. */
#define uF_Mod(a, b)    ((a) % (b))

/** \brief Divides a number by a power of two. */
#define uF_D2n(a, b)    ((a) >> (b))

/** \brief Multiplies a number by a power of two. */
#define uF_M2n(a, b)    ((a) << (b))

/** \brief Returns the absolute value of a given number. */
#define uF_Abs(a)       ((a) < 0 ? -(a) : (a))

/** \brief Returns the minimum between a and b. */
#define uF_Min(a, b)    ((a) <= (b) ? (a) : (b))

/** \brief Returns the maximum between a and b. */
#define uF_Max(a, b)    ((a) >= (b) ? (a) : (b))

/** \brief Creates a real number from an integer. */
#define uF_FPMake(a)    ((a) << uF_FP_FRACTIONAL_BITS)

/** \brief Real representation of the unit. */
#define uF_FP_UNIT         (1 << uF_FP_FRACTIONAL_BITS)

/*==================[typedef]================================================*/

/** \brief Fixed point type. */
typedef short uF_FP;

/** \brief Fixed point with double size. */
typedef int_fast32_t uF_FP_DOUBLE;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** \brief Converts a fixed point number into an integer in a given range.
 **
 ** Converts a fixed point number, which must belong to [0, 1), into an integer
 ** in [range_min, range_max).
 **
 ** \param fp A fixed point number such as 0 <= fp < 1
 ** \param range_min Minimum value for the integer.
 ** \param range_max Maximum value for the integer.
 ** \return An integer between range_min and range_max
 **/
short uF_FpToInt(uF_FP fp, short range_min, short range_max);

/** \brief Converts an integer in a given range to a fixed point number in
 ** [0, 1).
 **
 ** Converts an integer number, which must belong to [range_min, range_max),
 ** into a fixed point number in [0, 1).
 **
 ** \param i The integer.
 ** \param range_min Minimum value for the integer.
 ** \param range_max Maximum value for the integer.
 ** \return A fixed point number in [0, 1).
 **/
uF_FP uF_IntToFp(short i, short range_min, short range_max);

/** \brief Converts a fixed point number into an integer in a given range.
 **
 ** Converts a fixed point number, which must belong to [0, 1), into an integer
 ** in [range_min, range_max).
 **
 ** \param fp A fixed point number such as 0 <= fp < 1
 ** \param range_min Minimum value for the integer.
 ** \param range_max Maximum value for the integer.
 ** \return An integer between range_min and range_max
 **/
long uF_FpToLongInt(uF_FP fp, long range_min, long range_max);

/** \brief Converts an integer in a given range to a fixed point number in
 ** [0, 1).
 **
 ** Converts an integer number, which must belong to [range_min, range_max),
 ** into a fixed point number in [0, 1).
 **
 ** \param i The integer.
 ** \param range_min Minimum value for the integer.
 ** \param range_max Maximum value for the integer.
 ** \return A fixed point number in [0, 1).
 **/
uF_FP uF_LongIntToFp(long i, long range_min, long range_max);
/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif
/*==================[end of file]============================================*/
#endif /* #ifndef uF_FIXED_POINT_H */
