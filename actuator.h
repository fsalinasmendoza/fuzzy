#ifndef uF_ACTUATOR_H
#define uF_ACTUATOR_H

#include "macros.h"
#include "ruleset.h"

typedef struct mem_func_type mem_func_type;

typedef struct actuator_type
{
   mem_func_type* mf[ uF_ACTUATOR_MF_NUM];
   ruleset_type ruleset;
   uint_fast8_t mem_func_num;
   uint_fast8_t inference_type;
} actuator_type;

#endif // uF_ACTUATOR_H
