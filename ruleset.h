#ifndef uF_RULESET_H
#define uF_RULESET_H

#include "macros.h"

#define uF_RULESET_HEADER_SIZE		1

typedef struct ruleset_struct_type ruleset_struct_type;
typedef struct sensor_type sensor_type;

typedef struct ruleset_type
{
   uint8_t rule_num;
   uint8_t *data;
} ruleset_type;

/** \brief Initializes a ruleset structure with the data provided. 
 **
 ** \param rs Pointer where the initialized ruleset is returned.
 ** \param data Pointer to the ruleset raw data.
 ** \param sensor_num Number of sensors in the current controller.
 ** \param actuator_num Number of actuators in the current controller.
 ** \return Number of bytes taken from the data buffer.
 **/
int_fast8_t uF_ruleset_mamdani_init(ruleset_type *rs, uint8_t *data,
      uint_fast8_t sensor_num, uint_fast8_t actuator_num);
int_fast8_t uF_ruleset_sugeno_init(ruleset_type *rs, uint8_t *data,
      uint_fast8_t sensor_num, uint_fast8_t actuator_num);
void uF_ruleset_clearAll(void);

#endif // uF_RULESET_H
