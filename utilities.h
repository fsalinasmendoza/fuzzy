/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_UTILITIES_H
#define uF_UTILITIES_H

/*==================[inclusions]=============================================*/
#include "config.h"
#include "datatypes.h" 
#include "fixed_point.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/
#define _uF_STRING2( X ) #X
#define uF_STRING( X ) _uF_STRING2( X )

#define uF_POW0( a )    1
#define uF_POW1( a )    (a * uF_POW0( a ))
#define uF_POW2( a )    (a * uF_POW1( a ))
#define uF_POW3( a )    (a * uF_POW2( a ))
#define uF_POW4( a )    (a * uF_POW3( a ))
#define uF_POW5( a )    (a * uF_POW4( a ))
#define uF_POW6( a )    (a * uF_POW5( a ))
#define uF_POW7( a )    (a * uF_POW6( a ))
#define uF_POW8( a )    (a * uF_POW7( a ))
#define uF_POW9( a )    (a * uF_POW8( a ))
#define uF_POW10( a )   (a * uF_POW9( a ))
#define uF_POW11( a )   (a * uF_POW10( a ))
#define uF_POW12( a )   (a * uF_POW11( a ))
#define uF_POW13( a )   (a * uF_POW12( a ))
#define uF_POW14( a )   (a * uF_POW13( a ))
#define uF_POW15( a )   (a * uF_POW14( a ))
#define uF_POW16( a )   (a * uF_POW15( a ))
#define _uF_POW( a ,b ) uF_POW ## b ## ((a))
#define uF_POW( a,b )  _uF_POW( a,b )

#ifdef uF_NO_LIBS
#define uF_print( X )
#endif

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

#ifndef uF_NO_LIBS
void uF_print(const char *);
#endif

/** \brief Converts a 16 bits integer from big endiann to host byte order.
 ** \param s 16 bits integer
 ** \return s converted to host byte order.
 **/
uF_FP uF_Ntohs(int16_t s);

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* uF_UTILITIES_H */
