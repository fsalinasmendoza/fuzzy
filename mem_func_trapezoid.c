/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/*==================[inclusions]=============================================*/
#include "mem_func_trapezoid.h"

/* Number of sensors, actuators, etc. */
#include "config.h"
#include "fixed_point.h"
#include "mem_func.h"
/* Ntohs. */
#include "utilities.h"

/*==================[macros and definitions]=================================*/
/** \brief Maximum possible number of membership functions. */
#define MAX_MEM_FUNC (uF_ACTUATOR_NUM * uF_ACTUATOR_MF_NUM + uF_SENSOR_NUM * uF_SENSOR_MF_NUM)

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/** \brief Membership function pool. */
static mem_func_trapezoid_type _block[MAX_MEM_FUNC];

/** \brief Number of membership functions created. */
static int mem_func_counter = 0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

mem_func_trapezoid_type *uF_new_trapezoid(trapezoid_struct_type *ts)
{
   mem_func_trapezoid_type *mf;

   if (mem_func_counter >= MAX_MEM_FUNC)
   {
      return NULL;
   }
   mf = &_block[mem_func_counter++];

   mf->super.eval = uF_eval_trapezoid;
   mf->super.del = uF_delete_trapezoid;
   mf->point_x[0] = uF_Ntohs(ts->point_x[0]);
   mf->point_x[1] = uF_Ntohs(ts->point_x[1]);
   mf->point_x[2] = uF_Ntohs(ts->point_x[2]);
   mf->point_x[3] = uF_Ntohs(ts->point_x[3]);

   if (mf->point_x[0] < 0|| mf->point_x[0] >= uF_FP_UNIT ||
   mf->point_x[1] < 0 || mf->point_x[1] >= uF_FP_UNIT ||
   mf->point_x[2] < 0 || mf->point_x[2] >= uF_FP_UNIT ||
   mf->point_x[3] < 0 || mf->point_x[3] >= uF_FP_UNIT)
   {
      return NULL;
   }
   return mf;
}

void uF_delete_trapezoid(void *mf)
{

}

uF_FP uF_eval_trapezoid(void *mf, uF_FP x)
{
   uF_FP *point_x = ((mem_func_trapezoid_type *) mf)->point_x;
   uF_FP_DOUBLE x1;
   uF_FP_DOUBLE x2;

   if (x < point_x[0] || x > point_x[3])
   {
      return 0;
   }
   if (x > point_x[1] && x < point_x[2])
   {
      return uF_FP_UNIT;
   }

   x1 = point_x[0];
   x2 = point_x[1];
   if (x > point_x[1])
   {
      x1 = point_x[3];
      x2 = point_x[2];
   }

   /* If both are different. */
   if (x1 != x2)
   {
      /* The division can be performed. */
      return uF_Div((x - x1), (x2 - x1));
   }
   /* Else the slope is infinite */

   /* If the points belongs to [1, 254] then return 0.5 else return 1 */
   return (x1 == 0 || x1 == uF_FP_UNIT) ? uF_FP_UNIT : uF_D2n(uF_FP_UNIT, 1);
}

/*==================[end of file]============================================*/
