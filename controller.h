/* Copyright (c) 2015, Franco Javier Salinas Mendoza
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifndef uF_CONTROLLER_H
#define uF_CONTROLLER_H

/*==================[inclusions]=============================================*/
#include "macros.h"
#include "utilities.h"
#include "actuator.h"
#include "sensor.h"
#include "fixed_point.h"
#include "memory_struct.h"
#include "ruleset.h"
#include "tnorm.h"
#include "snorm.h"

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
extern "C"
{
#endif

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef struct controller_struct_type controller_struct_type;

typedef struct controller_type
{
   controller_struct_type *config;
   sensor_type sensors[uF_SENSOR_NUM];
   actuator_type actuators[uF_ACTUATOR_NUM];
   uF_FP input_weights[uF_SENSOR_NUM * uF_SENSOR_MF_NUM];
   uint_fast8_t mem_func_num[uF_SENSOR_NUM];
   uint_fast8_t input_offsets[uF_SENSOR_NUM];
} controller_type;

/*==================[external data declaration]==============================*/

extern uF_FP (*uF_and)(uF_FP, uF_FP);
extern uF_FP (*uF_or)(uF_FP, uF_FP);
extern uF_FP (*uF_activation)(uF_FP, uF_FP);
extern uF_FP (*uF_aggregation)(uF_FP, uF_FP);

/*==================[external functions declaration]=========================*/

/** \brief Creates a controller.
 **
 ** \param controller_struct Reference to the memory block which contains the
 ** controller data.
 ** \return Returns an initialized controller on success, NULL on error.
 **/
controller_type* uF_controller_init(void *controller_struct);

/** \brief Clears a controller.
 ** \param controller The controller to be cleared.
 **/
void uF_controller_deinit(controller_type *controller);

/** \brief Calculates the outputs of the fuzzy system.
 **
 ** \param controller Reference to the controller returned by uF_init.
 ** \param inputs Array with the inputs.
 ** \param inputs_size Size of the input array.
 ** \param outputs Array with the outputs.
 ** \param outputs_size Size of the output array.
 ** \return 0 on success, the error code on failure.
 **/
int uF_process(controller_type *controller, uF_FP *inputs, int inputs_size,
      uF_FP *outputs, int outputs_size);

/*==================[cplusplus]==============================================*/
#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* uF_CONTROLLER_H */
